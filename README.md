# Solutions des TP : "Persistance des données avec Hibernate et Spring"

## Prérequis

### Les indispensables :

* [Kit de développement Java (JDK) 8](http://www.oracle.com/technetwork/java/javase/downloads/index.html) - Bibliothèques logicielles Oracle de base du langage de programmation Java.
* [Maven 3+](https://maven.apache.org/) - Maven est un outil de construction de projets (build) open source développé par la fondation Apache.

### Les éléments facultatifs :

* [Spring Tool Suite](https://spring.io/tools/sts/all) - SpringSource ToolSuite, environnement de développement basé sur Eclipse, et recommandé pour tout développement Spring.
* [MySQL](https://dev.mysql.com/downloads/) - Système de gestion de bases de données relationnelles (SGBDR)

### Construction

Pour compiler les projets (depuis la racine du projet) :

```
mvn build
```

## Références

* [Java 8 Documentation](https://docs.oracle.com/javase/8/docs/) - Documentation Java 8.
* [Maven Documentation](http://maven.apache.org/guides/) - Documentation Apache Maven.
* [Hibernate Documentation](http://hibernate.org/orm/documentation) - Documentation du Framework hibernate.
* [Spring Documentation](https://docs.spring.io/spring/docs/) - Documentation du Framework Spring.
* [MySQL - Employees Sample Database](https://dev.mysql.com/doc/employee/en/) - Base de données 'Employees'.


