package fr.formations.tp1;

/**
 * Execution : mvn exec:java
 */
public class App {

	public static void main(String[] args) throws Exception {

		// Chargement du driver JDBC pour MySQL

		// url de connexion à la base de données
		String url = "jdbc:mysql://localhost:3306/tp1?useSSL=false";
		// utilisateur
		String user = "user";
		// mot de passe
		String password = "test";

		// Etape 1 : Mise en place de la connexion

		// Etape 2 : Allouer un objet de type 'Statment' (commande)

		// Etape 3 : Executer une requête SQL SELECT, et récupérer le résultat dans un objet 'ResultSet'
		// construction de la requête
		String strSelect = "SELECT id, name, role FROM employee";

		// Afficher la requête executée
		System.out.println("La requête SQL est : " + strSelect);
		// Executer la requête

		// Etape 4 : parcourir le 'ResultSet' en déplaçant le curseur avec la méthode 'next()'
		int rowCount = 0;

		System.out.println("Nombre total d'enregistrements : " + rowCount);

		// Etape 5: Fermeture et libération des ressources

	}
}
