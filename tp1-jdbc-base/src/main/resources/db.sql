DROP TABLE IF EXISTS `employee`;
CREATE TABLE `employee` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  `role` varchar(20) DEFAULT NULL,
  `insert_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

INSERT INTO `employee` (name, role, insert_time) VALUES ('David Dupont','Développeur', NOW());
INSERT INTO `employee` (name, role, insert_time) VALUES ('Fabrice Penot','CEO', NOW());
INSERT INTO `employee` (name, role, insert_time) VALUES ('Olivier Venati','CTO', NOW());
