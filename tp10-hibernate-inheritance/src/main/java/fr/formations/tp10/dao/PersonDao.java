
package fr.formations.tp10.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import fr.formations.tp10.model.Person;
import fr.formations.tp10.util.HibernateUtil;

public class PersonDao {

	SessionFactory sessionFactory;

	public PersonDao() {
		sessionFactory = HibernateUtil.getSessionFactory();
	}

	public void delete(Integer id) {
		Transaction transaction = null;
		Session session = sessionFactory.openSession();
		try {
			transaction = session.beginTransaction();
			Person e = (Person) session.get(Person.class, id);
			session.delete(e);
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	public Person getById(Integer id) {
		Session session = sessionFactory.openSession();
		return (Person) session.get(Person.class, id);
	}

	public Person getByName(String name) {
		Session session = sessionFactory.openSession();
		Person result = null;
		try {
			result = (Person) session.createCriteria(Person.class).add(Restrictions.like("name", name)).uniqueResult();
		} catch (HibernateException e) {
			e.printStackTrace();
		} finally {
			session.close();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<Person> list() {
		Session session = sessionFactory.openSession();
		List<Person> result;
		try {
			result = session.createCriteria(Person.class).list();
		} catch (HibernateException e) {
			e.printStackTrace();
			result = new ArrayList<Person>();
		} finally {
			session.close();
		}
		return result;
	}

	public Integer save(Person Person) {
		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		Integer id = null;
		try {
			transaction = session.beginTransaction();
			id = (Integer) session.save(Person);
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return id;
	}

}
