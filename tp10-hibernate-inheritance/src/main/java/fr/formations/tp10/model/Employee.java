package fr.formations.tp10.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("E")
public class Employee extends Person {

	@Column(name = "role", length = 20, nullable = true)
	private String role;

	public Employee() {
		super();
	}

	public Employee(String name, String role) {
		super(name);
		this.role = role;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

}
