package fr.formations.tp10.dao;

import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import fr.formations.tp10.dao.PersonDao;
import fr.formations.tp10.model.Employee;
import fr.formations.tp10.model.Person;

public class PersonDaoTest {

	static PersonDao personDao;

	@BeforeClass
	public static void beforeEach() {
		personDao = new PersonDao();
	}

	@Test
	public void testSave() {

		Person person1 = new Person("Jean");
		personDao.save(person1);
		assertNotNull(person1.getId());

		Employee employee1 = new Employee("Christian", "Développeur Java");
		Employee employee2 = new Employee("David", "Développeur NodeJS");
		Employee employee3 = new Employee("Charles", "Développeur .Net");
		Employee employee4 = new Employee("Jérôme", "Freelance");

		personDao.save(employee1);
		personDao.save(employee2);
		personDao.save(employee3);
		personDao.save(employee4);
		assertNotNull(employee1.getId());
		assertNotNull(employee2.getId());
		assertNotNull(employee3.getId());
		assertNotNull(employee4.getId());
	}

	@Before
	public void setUp() throws Exception {

	}

	@After
	public void tearDown() throws Exception {
	}

}
