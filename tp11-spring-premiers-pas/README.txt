TP Spring : premiers pas

Objectifs :
 - créer un projet Spring sous Eclipse
 - créer des beans
 - injecter des dépendances

Etapes :

 1 - Construire une classe MessageService avec une méthode qui publie un message.
 2 - Construire une classe Client avec attributs de type MessageService, qui récupère le message « hello world » publié par le bean messageService.
 3 - Déclarer et assembler ces beans dans un contexte XML Spring.
 4 - Créer une classe avec une méthode main qui lance Client.


Compiler, tester et exécuter le programme :
	mvn clean compile test exec:java