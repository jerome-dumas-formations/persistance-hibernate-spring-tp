TP Spring : premiers pas - création d’interface

Objectifs : - Approfondir sur l’injection de dépendances.
Etapes :

 1 - Transformer MessageService en une interface 2 - Créer 2 implémentations, une retournant « Hello » et une autre « World » 3 - Modifier le client pour qu’il appelle les 2 implémentations.


Compiler, tester et exécuter le programme :
	mvn clean compile test exec:java