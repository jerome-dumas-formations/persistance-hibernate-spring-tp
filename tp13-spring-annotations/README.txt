TP Spring : utilisation des annotations

Objectifs : - Limiter la configuration XML.
Etapes :

 1 - Utiliser les annotations @Autowired et @Component. 2 - Activer le scan des annotations.


Compiler, tester et exécuter le programme :
	mvn clean compile test exec:java