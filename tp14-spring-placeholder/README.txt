TP Spring : Configuration par placeholder

Objectifs : - Externaliser la configuration applicative du fichier XML Spring
Etapes :

 1 - Modifier MessageService pour configurer le contenu du message envoyé
 2 - Mettre les contenus dans un fichier properties

Compiler, tester et exécuter le programme :
	mvn clean compile test exec:java