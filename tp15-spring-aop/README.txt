TP Spring : AOP - jacadi

Objectifs : - Utiliser l’AOP pour ajouter un comportement à une classe existante
Etapes :

 1 - Implémenter un aspect qui ajoute « nouveau message: » au message enregistré par MessageService


Compiler, tester et exécuter le programme :
	mvn clean compile test exec:java