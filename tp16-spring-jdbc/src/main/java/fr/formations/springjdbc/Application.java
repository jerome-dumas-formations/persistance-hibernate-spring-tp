package fr.formations.springjdbc;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.formations.springjdbc.model.Person;
import fr.formations.springjdbc.service.PersonService;

public class Application {

	static final Logger logger = LoggerFactory.getLogger(Application.class);

	public static void main(String[] args) throws Throwable {
		ClassPathXmlApplicationContext appContext = new ClassPathXmlApplicationContext("applicationContext.xml");

		PersonService personService = (PersonService) appContext.getBean("personService");

		// supprimer les eventuelles personnes existantes dans la base
		logger.info("Suppression des personnes enregistrées en BDD.");
		List<Person> personsToDelete = personService.findAll();
		for (Person person : personsToDelete) {
			personService.deletePerson(person.getPersonId());
		}

		Person person1 = new Person(1, "Jean", "Dupont", 32);
		Person person2 = new Person(2, "Christian", "Rocket", 25);
		Person person3 = new Person(3, "Sylvain", "Mongo", 40);

		personService.addPerson(person1);
		personService.addPerson(person2);
		personService.addPerson(person3);

		List<Person> persons = personService.findAll();
		logger.info("Voici la liste des personnes enregistrées : " + persons);

		logger.info("Suppression de la personne ID=2");
		personService.deletePerson(2);

		logger.info("Mise à jour des infos de la personne ID=1");
		person1.setFirstName("Jean MAJ");
		person1.setLastName("Dupont MAJ");
		person1.setAge(40);
		personService.updatePerson(person1, 1);

		logger.info("Rechercher la personne ID=3 -> " + personService.find(3));

		logger.info("Voici à nouveau la liste des personnes enregistrées : " + personService.findAll());

		appContext.close();
	}
}
