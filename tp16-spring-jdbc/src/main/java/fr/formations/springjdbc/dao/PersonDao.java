package fr.formations.springjdbc.dao;

import java.util.List;

import fr.formations.springjdbc.model.Person;

public interface PersonDao {

	public void addPerson(Person person);

	public void updatePerson(Person person, int personId);

	public void deletePerson(int personId);

	public Person find(int personId);

	public List<Person> findAll();
}