package fr.formations.springjdbc.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import fr.formations.springjdbc.model.Person;

@Repository
@Qualifier("personDao")
public class PersonDaoImpl implements PersonDao {

// TODO: Compléter avec un jdbcTemplate et le corps des méthodes
	public void addPerson(Person person) {
	}

	public void updatePerson(Person person, int personId) {
	}

	public void deletePerson(int personId) {
	}

	public Person find(int personId) {
	}

	public List<Person> findAll() {
	}
}