package fr.formations.springjdbc.service;

import java.util.List;

import fr.formations.springjdbc.model.Person;

public interface PersonService {

	public void addPerson(Person person);

	public void updatePerson(Person person, int personId);

	public void deletePerson(int personId);

	public Person find(int personId);

	public List<Person> findAll();
}