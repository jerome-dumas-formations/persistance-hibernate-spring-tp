package fr.formations.springjdbc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.formations.springjdbc.dao.PersonDao;
import fr.formations.springjdbc.model.Person;

@Service("personService")
public class PersonServiceImpl implements PersonService {

// TODO: ajouter la référence au DAO + corps des méthodes

	public void addPerson(Person person) {

	}

	public void updatePerson(Person person, int personId) {

	}

	public void deletePerson(int personId) {

	}

	public Person find(int personId) {

	}


	}
}
