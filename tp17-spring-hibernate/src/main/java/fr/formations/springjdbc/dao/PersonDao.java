package fr.formations.springjdbc.dao;

import java.util.List;

import fr.formations.springjdbc.model.Person;

public interface PersonDao {

	public void savePerson(Person person);

	public void updatePerson(Person person);

	public void deletePerson(Person person);
	public void deletePerson(int personId);

	public Person find(int personId);

	public List<Person> findAll();
}