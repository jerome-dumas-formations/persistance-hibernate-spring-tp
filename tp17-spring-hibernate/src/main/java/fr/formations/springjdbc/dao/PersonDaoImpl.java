package fr.formations.springjdbc.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import fr.formations.springjdbc.model.Person;

@Repository
@Qualifier("personDao")
public class PersonDaoImpl extends AbstractDao implements PersonDao {

	//TODO: corps des méthodes à compléter


	public void savePerson(Person person) {

	}

	public void updatePerson(Person person) {

	}

	public void deletePerson(Person person) {

	}

	public void deletePerson(int personId) {

	}

	public Person find(int personId) {

	}

	public List<Person> findAll() {

	}
}