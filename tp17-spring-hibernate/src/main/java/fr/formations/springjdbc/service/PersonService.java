package fr.formations.springjdbc.service;

import java.util.List;

import javax.transaction.Transactional;

import fr.formations.springjdbc.model.Person;

@Transactional
public interface PersonService {

	public void savePerson(Person person);

	public void updatePerson(Person person);

	public void deletePerson(Person person);
	public void deletePerson(int personId);

	public Person find(int personId);

	public List<Person> findAll();
}