package fr.formations.springjdbc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.formations.springjdbc.dao.PersonDao;
import fr.formations.springjdbc.model.Person;

@Service("personService")
public class PersonServiceImpl implements PersonService {


	public void savePerson(Person person) {

	}

	public void updatePerson(Person person) {

	}

	public void deletePerson(int personId) {

	}

	public void deletePerson(Person person) {

	}

	public Person find(int personId) {

	}

	public List<Person> findAll() {

	}
}
