package fr.formations.tp2;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Programme de démonstration de JDBC.
 * Base de données de la consommation de café de différents développeurs.
 *
 * Execution : mvn exec:java
 */
public class App {

	/**
	 * Connexion à la BDD.
	 */
	private static Connection con;

	public static void main(String[] args) throws Exception {

		// construction de notre object App
		App application = new App("application.properties");

		// suppression de la table si elle existe
		application.supprimerTable();

		// création de la table
		application.creerTable();

		// charger les données à partir d'un fichier
		application.chargerBase("data.db");

		// déterminer le plus grand consommateur de cafés
		application.trouverMaxCafe();

		// validation manuelle des modifications sur la base
		con.commit();

		// enregistrer des consommations supplémentaire à un utilisateur
		System.out.println("Enregistrement des consommations de Sylvain.");
		application.enregistrerConsommations("Sylvain", Date.valueOf("2017-07-14"), 5);

		// déterminer le plus grand consommateur de cafés
		application.trouverMaxCafe();

		// annulation des modifications sur la transaction en cours
		System.out.println("Annulation de l'enregistrement des consommations de Sylvain.");
		con.rollback();

		// déterminer le plus grand consommateur de cafés
		application.trouverMaxCafe();

		// fermer la connexion
		application.fermerConnexion(con);
	}

	public App(final String fichierProperties) {

		Properties properties = new Properties();

		// chargement des propriétés définis dans le fichier
		try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(fichierProperties)) {
			properties.load(inputStream);
		} catch (FileNotFoundException fne) {
			System.out.println("Fichier " + fichierProperties + " non trouvé.");
			System.exit(0);
		} catch (IOException ioe) {
			System.out.println("Problème lecture du ficher properties: " + ioe.getMessage());
			System.exit(0);
		}

		String driverClassName = properties.getProperty("jdbc.driverClassName");
		String url = properties.getProperty("jdbc.url");
		String username = properties.getProperty("jdbc.username");
		String password = properties.getProperty("jdbc.password");

		// chargement du driver
		chargerDriver(driverClassName);

		// ouverture d'une connexion
		con = ouvrirConnexion(url, username, password);
	}

	/**
	 * Charger les données dans la BDD.
	 *
	 * @throws URISyntaxException
	 */
	private void chargerBase(final String nomFichier) throws URISyntaxException {
		System.out.println("Chargement des données à partir du fichier '" + nomFichier + "'.");

	}

	/**
	 * Chargement du driver JDBC.
	 *
	 * @param driver
	 *            name le nom (Fully qualified Name) de la classe du driver.
	 */
	private void chargerDriver(String driverName) {

	}

	/**
	 * Création de la table consommation.
	 */
	private void creerTable() throws SQLException {

	}

	private void enregistrerConsommations(String developpeur, Date date, int nbCafe) {

	}

	private void fermerConnexion(Connection con) {

	}

	/**
	 * Création d'une connexion.
	 *
	 * @param url
	 *            l'url de la BDD.
	 * @param username
	 *            le nom de l'utilisateur
	 * @param passwd
	 *            son mot de passe.
	 * @param return
	 *            la ref de l'objet connexion créé.
	 */
	private Connection ouvrirConnexion(String url, String username, String passwd) {

		return null;
	}

	/**
	 * Suppression de la table consommation.
	 */
	private void supprimerTable() {

	}

	/**
	 * Déterminer le plus grand consommateur de café.
	 */
	private void trouverMaxCafe() {

	}
}
