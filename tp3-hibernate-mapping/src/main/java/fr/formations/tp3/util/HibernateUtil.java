package fr.formations.tp3.util;

import org.hibernate.SessionFactory;

public class HibernateUtil {

	private static SessionFactory sessionFactory;

	private static SessionFactory buildSessionFactory() {
		try {
			// créer la SessionFactory à partir du fichier spécifié

			return sessionFactory;
		} catch (Throwable ex) {
			System.err.println("Echec de création de la SessionFactory : " + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}

	public static SessionFactory getSessionFactory() {
		if (sessionFactory == null) {
			sessionFactory = buildSessionFactory();
		}

		return sessionFactory;
	}
}
