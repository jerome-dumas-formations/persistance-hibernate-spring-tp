
package fr.formations.tp5.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import fr.formations.tp5.model.Employee;
import fr.formations.tp5.util.HibernateUtil;

public class EmployeeDao {

	SessionFactory sessionFactory;

	public EmployeeDao() {
		sessionFactory = HibernateUtil.getSessionFactory();
	}

	public Integer save(Employee employee) {
		return null;
	}

	public List<Employee> list() {
		return null;
	}

	public Employee getById(Integer id) {
		Session session = sessionFactory.openSession();
		return (Employee) session.get(Employee.class, id);
	}

	public Employee getByName(String name) {
		return null;
	}

	public void delete(Integer id) {

	}

}
