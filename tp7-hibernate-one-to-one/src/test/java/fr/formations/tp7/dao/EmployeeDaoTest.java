package fr.formations.tp7.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import java.sql.DriverManager;

import org.apache.derby.tools.sysinfo;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import fr.formations.tp7.dao.EmployeeDao;
import fr.formations.tp7.model.Address;
import fr.formations.tp7.model.Employee;

public class EmployeeDaoTest {

	static final int NB_EMPLOYEES = 5;
	static EmployeeDao employeeDao;

	@BeforeClass
	public static void beforeEach() {
		employeeDao = new EmployeeDao();

		for (int i = 0; i < NB_EMPLOYEES; i++) {
			Address address = new Address("Adress name" + i, "Street " + i, "44000", "Nantes");

			// TODO : Affecter une adresse à l'employer
			Employee employee = new Employee("Employee" + i, "Développeur");
			employeeDao.save(employee);
		}
	}

	@Before
	public void setUp() throws Exception {

	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testList() {
		assertEquals(employeeDao.list().size(), NB_EMPLOYEES);
	}

	@Test
	public void testGetByName() {
		assertNotNull(employeeDao.getByName("Employee1"));
	}

	@Test
	public void testGetByAdressName() {
		assertNotNull(employeeDao.getByAddressName("Adress name1"));
	}

	@Test
	public void testDelete() {
		Employee employee1 = employeeDao.getByName("Employee1");
		assertNotNull(employee1);
		employeeDao.delete(employee1.getId());

		assertNull(employeeDao.getByName("Employee1"));
	}

}
