
package fr.formations.tp8.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import fr.formations.tp8.model.Employee;
import fr.formations.tp8.util.HibernateUtil;

public class EmployeeDao {

	SessionFactory sessionFactory;

	public EmployeeDao() {
		sessionFactory = HibernateUtil.getSessionFactory();
	}

	public void delete(Integer id) {
		Transaction transaction = null;
		Session session = sessionFactory.openSession();
		try {
			transaction = session.beginTransaction();
			Employee e = (Employee) session.get(Employee.class, id);
			session.delete(e);
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	public Employee getById(Integer id) {
		Session session = sessionFactory.openSession();
		return (Employee) session.get(Employee.class, id);
	}

	public Employee getByName(String name) {
		Session session = sessionFactory.openSession();
		Employee result = null;
		try {
			result = (Employee) session.createCriteria(Employee.class).add(Restrictions.like("name", name)).uniqueResult();
		} catch (HibernateException e) {
			e.printStackTrace();
		} finally {
			session.close();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<Employee> list() {
		Session session = sessionFactory.openSession();
		List<Employee> result;
		try {
			result = session.createCriteria(Employee.class).list();
		} catch (HibernateException e) {
			e.printStackTrace();
			result = new ArrayList<Employee>();
		} finally {
			session.close();
		}
		return result;
	}

	public Integer save(Employee employee) {
		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		Integer id = null;
		try {
			transaction = session.beginTransaction();
			id = (Integer) session.save(employee);
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return id;
	}

}
