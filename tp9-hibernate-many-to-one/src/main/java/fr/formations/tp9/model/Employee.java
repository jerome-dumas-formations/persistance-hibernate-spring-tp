package fr.formations.tp9.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "Employee", uniqueConstraints = { @UniqueConstraint(columnNames = { "id" }) })
public class Employee {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true, length = 11)
	private int id;

	@Column(name = "insert_time", nullable = true)
	private Date insertTime;

	@Column(name = "name", length = 20, nullable = true, unique = true)
	private String name;

	@Column(name = "role", length = 20, nullable = true)
	private String role;

	// TODO: Ajouter un employeur à vos employés + getter/setter

	public Employee() {
	}

	public Employee(String name, String role) {
		super();
		this.name = name;
		this.role = role;
		this.insertTime = new Date();
	}

	public int getId() {
		return id;
	}

	public Date getInsertTime() {
		return insertTime;
	}

	public String getName() {
		return name;
	}

	public String getRole() {
		return role;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setInsertTime(Date insertTime) {
		this.insertTime = insertTime;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setRole(String role) {
		this.role = role;
	}

}
