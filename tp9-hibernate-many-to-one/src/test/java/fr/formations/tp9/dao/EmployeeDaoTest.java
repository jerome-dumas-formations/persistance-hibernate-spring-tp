package fr.formations.tp9.dao;

import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import fr.formations.tp9.model.Employee;

public class EmployeeDaoTest {

	static EmployeeDao employeeDao;

	@BeforeClass
	public static void beforeEach() {
		employeeDao = new EmployeeDao();
	}

	@Test
	public void testSave() {

		Employee employee1 = new Employee("Christian", "Développeur Java");
		Employee employee2 = new Employee("David", "Développeur NodeJS");
		Employee employee3 = new Employee("Charles", "Développeur .Net");
		Employee employee4 = new Employee("Jérôme", "Freelance");

		// TODO: Associer un employeur à vos employés

		// vérifier que l'identifiant n'a pas été défini
		// TODO : tester que votre employeur à un ID null

		employeeDao.save(employee1);
		// TODO : tester que votre employeur à un ID non null

		employeeDao.save(employee2);
		employeeDao.save(employee3);
		employeeDao.save(employee4);
		assertNotNull(employee1.getId());
		assertNotNull(employee2.getId());
		assertNotNull(employee3.getId());
		assertNotNull(employee4.getId());

		// TODO: vérifier que l'employeur de "employee1" est le même que "employee2"
	}

	@Before
	public void setUp() throws Exception {

	}

	@After
	public void tearDown() throws Exception {
	}

}
